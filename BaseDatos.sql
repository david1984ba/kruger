-- Ejecutar en con usuario root

DROP DATABASE IF EXISTS inventario_db;
create  DATABASE inventario_db;
DROP ROLE IF EXISTS inventario_usr;
CREATE ROLE inventario_usr WITH 
	LOGIN ENCRYPTED PASSWORD '1nv3nt4r10'
	SUPERUSER
	NOCREATEDB
	CREATEROLE
	INHERIT
	NOREPLICATION
	NOBYPASSRLS
	CONNECTION LIMIT -1;

--------------------------------------------------
-- Ejecutar con usuario inventario_usr

DROP SCHEMA IF EXISTS inventario CASCADE;
CREATE SCHEMA inventario AUTHORIZATION inventario_usr;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA inventario TO inventario_usr;

DROP TABLE IF EXISTS inventario.vacuna;

CREATE TABLE inventario.vacuna (
	id_vacuna serial4 NOT NULL,
	nombre_vacuna varchar(255) NULL,
	CONSTRAINT vacuna_pkey PRIMARY KEY (id_vacuna)
);

DROP TABLE IF EXISTS inventario.empleado;

CREATE TABLE inventario.empleado (
	cedula varchar(255) NOT NULL,
	apellidos varchar(255) NULL,
	celular varchar(255) NULL,
	correo_electronico varchar(255) NULL,
	domicilio varchar(255) NULL,
	estado_vacunacion varchar(255) NULL,
	fecha_nacimiento date NULL,
	fecha_vacunacion date NULL,
	nombres varchar(255) NULL,
	numero_dosis int4 NULL,
	id_vacuna int4 NOT NULL,
	CONSTRAINT empleado_pkey PRIMARY KEY (cedula)	
);


DROP TABLE IF EXISTS inventario.user_role;

CREATE TABLE inventario.user_role (
	user_id int4 NOT NULL,
	role_id int4 NOT NULL,
	CONSTRAINT user_role_pkey PRIMARY KEY (user_id, role_id)
);

DROP TABLE IF EXISTS inventario.role;

CREATE TABLE inventario.role (
	role_id serial4 NOT NULL,
	role varchar(255) NULL,
	CONSTRAINT role_pkey PRIMARY KEY (role_id)
);


DROP TABLE IF EXISTS inventario.user;

CREATE TABLE inventario.user (
	user_id serial4 NOT NULL,
	username varchar(255) NULL,
	password varchar(255) NULL,
	cedula varchar(255) NULL,
	CONSTRAINT user_pkey PRIMARY KEY (user_id)
);


ALTER TABLE inventario.empleado ADD CONSTRAINT fk_empleado_vacuna FOREIGN KEY (id_vacuna) REFERENCES inventario."vacuna"(id_vacuna) ON DELETE CASCADE;;
ALTER TABLE inventario.user_role ADD CONSTRAINT fk_user_rol_user FOREIGN KEY (user_id) REFERENCES inventario."user"(user_id) ON DELETE CASCADE;
ALTER TABLE inventario.user_role ADD CONSTRAINT fk_user_rol_rol  FOREIGN KEY (role_id) REFERENCES inventario."role"(role_id) ON DELETE CASCADE;
ALTER TABLE inventario.user      ADD CONSTRAINT fk_user_empleado FOREIGN KEY (cedula) REFERENCES inventario.empleado(cedula) ON DELETE CASCADE;

INSERT INTO inventario."role" (role_id, "role") VALUES(1, 'ADMIN');
INSERT INTO inventario."role" (role_id, "role") VALUES(2, 'USER');

--Clave es un hash bcrypt de la palabra admin
INSERT INTO inventario.user( "username", "password", "cedula")
VALUES( 'admin', '$2a$12$SigMZzPP/h8eAt2GVCw6gOtDfJSRbYFgyBngoe0NRXpq4Yd5671uq', null);

-- Asiga un rol ADMIN a admin
INSERT INTO inventario.user_role (user_id, role_id) VALUES((select user_id from inventario."user" u where u.username = 'admin'), 1);
-- Asiga un rol USER a admin
INSERT INTO inventario.user_role (user_id, role_id) VALUES((select user_id from inventario."user" u where u.username = 'admin'), 2);


-- Vacunas

INSERT INTO inventario.vacuna (id_vacuna, nombre_vacuna) VALUES(1, 'Ninguna');
INSERT INTO inventario.vacuna (id_vacuna, nombre_vacuna) VALUES(2, 'Sputnik');
INSERT INTO inventario.vacuna (id_vacuna, nombre_vacuna) VALUES(3, 'AstraZeneca');
INSERT INTO inventario.vacuna (id_vacuna, nombre_vacuna) VALUES(4, 'Pfizer');
INSERT INTO inventario.vacuna (id_vacuna, nombre_vacuna) VALUES(5, ' Jhonson&Jhonson');

