package com.kruger.inventario.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kruger.inventario.model.Empleado;
import com.kruger.inventario.model.EmpleadoFull;
import com.kruger.inventario.repository.EmpleadoRepository;
import com.kruger.inventario.service.IEmpleadoService;
import com.kruger.inventario.utils.Functions;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService {

	Logger logger = LoggerFactory.getLogger(EmpleadoServiceImpl.class);

	@Autowired
	private EmpleadoRepository empleadoRepository;

	@Autowired
	private Functions utils;

	@Override
	public List<EmpleadoFull> listar() {
		List<EmpleadoFull> empleados = new ArrayList<>();
		List<Object[]> eo = new ArrayList<>();
		eo = empleadoRepository.findByAll();
		llenarListaEmpleados(eo, empleados);
		return empleados;

	}

	@Override
	public HashMap<String, Object> guardar(Empleado empleado) {
		HashMap<String, Object> resultado = new HashMap<>();
		if (isValidado(empleado, "guardar")) {
			empleadoRepository.save(empleado);
			resultado = utils.generarCredendiales(empleado);
			return resultado;
		}
		return resultado;
	}

	@Override
	public List<EmpleadoFull> listarPorEstado(String estado_vacunacion) {
		List<EmpleadoFull> empleados = new ArrayList<>();
		List<Object[]> eo = new ArrayList<>();
		eo = empleadoRepository.findByEstado_vacunacion(estado_vacunacion.toUpperCase());
		llenarListaEmpleados(eo, empleados);
		return empleados;
	}

	@Override
	public List<EmpleadoFull> listarPorTipoVacuna(int id_vacuna) {
		List<EmpleadoFull> resultado = new ArrayList<>();
		List<Object[]> eo = new ArrayList<>();
		eo = empleadoRepository.findById_vacuna(id_vacuna);
		llenarListaEmpleados(eo, resultado);
		return resultado;
	}

	@Override
	public List<EmpleadoFull> listarPorNombreVacuna(String nombre_vacuna) {
		List<EmpleadoFull> resultado = new ArrayList<>();
		List<Object[]> eo = new ArrayList<>();
		eo = empleadoRepository.findByNombreVacuna(nombre_vacuna);
		llenarListaEmpleados(eo, resultado);
		return resultado;
	}

	@Override
	public List<EmpleadoFull> listarPorFecha(Date fecha_inicio, Date fecha_fin) {
		List<EmpleadoFull> resultado = new ArrayList<>();
		List<Object[]> eo = new ArrayList<>();
		eo = empleadoRepository.findByFecha_vacunacion(fecha_inicio, fecha_fin);
		llenarListaEmpleados(eo, resultado);
		return resultado;
	}

	@Override
	public Boolean eliminar(String cedula) {

		try {
			empleadoRepository.deleteById(cedula);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private Boolean isValidado(Empleado empleado, String tipo) {
		if (!utils.validarCedula(empleado.getCedula())) {
			return false;
		}
		if (!utils.validarCaracteres(empleado.getNombres())) {
			return false;
		}

		if (!utils.validarCaracteres(empleado.getApellidos())) {
			return false;
		}

		if (!utils.validarCorreo(empleado.getCorreo_electronico())) {
			return false;
		}

		if ("guardar".equalsIgnoreCase(tipo)) {
			empleado.setEstado_vacunacion("NO VACUNADO");
			empleado.setCorreo_electronico("");
			empleado.setCelular("");
			empleado.setFecha_nacimiento(null);
			empleado.setFecha_vacunacion(null);
			empleado.setNumero_dosis(0);
			empleado.setDomicilio("");
			empleado.setId_vacuna(1);
		} else {
			if (!utils.validarCaracteres(empleado.getDomicilio())) {
				return false;
			}
			if (!utils.validarFecha(empleado.getFecha_nacimiento())) {
				return false;
			}
			if (!utils.validarNumeros(String.valueOf(empleado.getCelular()), "celular")) {
				return false;
			}
			if ("VACUNADO".equalsIgnoreCase(empleado.getEstado_vacunacion().toUpperCase())) {
				empleado.setEstado_vacunacion(empleado.getEstado_vacunacion().toUpperCase());
				if (!utils.validarNumeros(String.valueOf(empleado.getNumero_dosis()), "dosis")) {
					return false;
				}
				if (!utils.validarFecha(empleado.getFecha_vacunacion())) {
					return false;
				}
			}

		}

		return true;

	}

	@Override
	public List<EmpleadoFull> listarPorCedulaEmpleado(String cedula) {
		List<Object[]> eo = new ArrayList<>();
		List<EmpleadoFull> resultado = new ArrayList<>();
		eo = empleadoRepository.findByCedula(cedula);
		llenarListaEmpleados(eo, resultado);
		return resultado;
	}

	@Override
	public HashMap<String, Object> editar(Empleado empleado) {
		HashMap<String, Object> resultado = new HashMap<>();
		if (isValidado(empleado, "editar")) {
			empleadoRepository.save(empleado);
			resultado.put("operacion","OK");
			return resultado;
		}
		return resultado;
	}

	private void llenarListaEmpleados(List<Object[]> empleadosO, List<EmpleadoFull> empleados) {
		for (Object[] var : empleadosO) {
			EmpleadoFull e = new EmpleadoFull();
			e.setCedula(var[0].toString());
			e.setNombres(var[1].toString());
			e.setApellidos(var[2].toString());
			e.setCorreo_electronico(var[3].toString());
			e.setFecha_nacimiento(var[4]==null?null:(Date.valueOf(var[4].toString())));
			e.setDomicilio(var[5].toString());
			e.setCelular(var[6].toString());
			e.setEstado_vacunacion(var[7].toString());
			e.setId_vacuna(Integer.parseInt(var[8].toString()));
			e.setNombre_vacuna(var[9].toString());
			e.setFecha_vacunacion(var[10] ==null?null:(Date.valueOf(var[10].toString())));
			e.setNumero_dosis(Integer.parseInt(var[11].toString()));
			empleados.add(e);
		}

	}
}
