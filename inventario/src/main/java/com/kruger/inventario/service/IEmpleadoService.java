package com.kruger.inventario.service;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;

import com.kruger.inventario.model.Empleado;
import com.kruger.inventario.model.EmpleadoFull;


public interface IEmpleadoService {
	List<EmpleadoFull> listar();
	HashMap<String, Object> guardar(Empleado empleado);
	List<EmpleadoFull>listarPorFecha(Date fecha_inicio, Date fecha_fin);
	Boolean eliminar(String cedula);
	List<EmpleadoFull> listarPorTipoVacuna(int id_vacuna);
	List<EmpleadoFull> listarPorEstado(String estado_vacunacion);
	List<EmpleadoFull> listarPorNombreVacuna(String nombre_vacuna);
	List<EmpleadoFull> listarPorCedulaEmpleado(String cedula);
	HashMap<String, Object> editar(Empleado empleado);

}
