package com.kruger.inventario.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.kruger.inventario.model.User;

public interface IUserService {
	UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

	void delete(long id);
	User save(User user);
}
