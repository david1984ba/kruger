package com.kruger.inventario.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kruger.inventario.model.Vacuna;
import com.kruger.inventario.repository.VacunaRepository;
import com.kruger.inventario.service.IVacunaService;

@Service
public class VacunaServiceImpl implements IVacunaService{

	@Autowired
	private VacunaRepository vacunaRepository;
	
	@Override
	public List<Vacuna> listar() {
		List<Vacuna> vacunas = new ArrayList<Vacuna>();
		vacunaRepository.findAll().forEach(vacunas::add);
		return vacunas;
	}

	@Override
	public boolean guardar(Vacuna vacuna) {
		try {
			vacunaRepository.save(vacuna);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
