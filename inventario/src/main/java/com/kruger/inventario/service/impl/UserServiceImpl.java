package com.kruger.inventario.service.impl;

import com.kruger.inventario.model.Role;
import com.kruger.inventario.model.User;
import com.kruger.inventario.repository.UserRepository;
import com.kruger.inventario.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;


@Service
public class UserServiceImpl implements UserDetailsService,IUserService {

    @Autowired
    private UserRepository userRepository;

	@Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findUserByUsername(username);
		
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority(user));
    }
	

    private List<SimpleGrantedAuthority> getAuthority(User user) {
    	List<SimpleGrantedAuthority> roles = new ArrayList<>();
    	for (Role r : user.getRoles()) {
			roles.add(new SimpleGrantedAuthority("ROLE_" + r.getRole().toUpperCase()));
		}
        return roles;
    }
    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        userRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
    	userRepository.deleteById((int) id);
    }
    
    @Override
    public User save(User user) {
        return userRepository.save(user);
    }    
}


