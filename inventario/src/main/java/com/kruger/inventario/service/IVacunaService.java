package com.kruger.inventario.service;

import java.util.List;
import com.kruger.inventario.model.Vacuna;


public interface IVacunaService {
	List<Vacuna> listar();
	boolean guardar(Vacuna vacuna);
	

}
