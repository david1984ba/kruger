package com.kruger.inventario.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kruger.inventario.model.Role;

@Repository
public interface RolRepository extends CrudRepository<Role, Integer>{
	
}
