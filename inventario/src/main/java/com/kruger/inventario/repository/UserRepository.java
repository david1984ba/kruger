package com.kruger.inventario.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kruger.inventario.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
	User  findUserByUsername (@Param("username") String username);
	User findByCedula (@Param("cedula") String cedula);
	
}
