# Inventario de vacunación de empleados

## Base de Datos

Para consumir la aplicación se requiere contar con una base de datos Postgres con su respectivo esquema y estructura de tablas.
Adjunto se encuentra el script ```BaseDatos.sql``` que contiene las sentencias necesarias para crear toda la estructura.
Tomar en cuenta que las primeras sentencias del archivo se deben ejecutar con el usuario con los privilegios para crear el usuario y la base de datos.
El resto de comandos adicionales se deben ejecutar dentro del usuario creado.

Adicional se adjunta el Diagrama Entidad Relacion de la base de datos.

Este script tambien inserta los datos para inicializar la aplicación, con lo cual se crea el usuario: admin, password: admin y los roles ADMIN y USER.

## Aplicación

Para poder obtener el codigo fuente de la aplicación se deben ejecutar el siguiente comando:

```
git clone https://gitlab.com/david1984ba/kruger.git
git checkout main
```
Es importante ejecutar la aplicación posterior a la creación de la base de datos para que esta pueda ser validada.

## Compilacion e importacion al IDE (Eclipse)

La aplicación se ha desarrollado sobre java 8, por lo que se recomienda disponer de esa versión.

Para la compilación se ejecutará el comando ```mvn clean install -DskipTests=true```. Al finalizar en eclipse se debe importar como proyecto maven existente.


## Swagger

La aplicación dispone de la documentación de las APIS por medio de swagger. Se pudo personalizar la configuracion para poder generar el token de autenticación
y consumir en la misma interfaz los servicios expuestos.

Para la generación del token se debe ingrear el usuario admin y la contraseña admin.


## Funcionamiento

La aplicacion esta desarrollada  para llevar un registro del inventario del estado de vacunación de los empleados.

Para lo cual se cuenta con 2 roles: Administrador y Empleado (ADMIN y USER respectivamente).

Con el usuario admin con rol ADMIN (se crea por defecto junto con la base de datos) se permite crear el registro del empleado, tomando en cuenta que los campos 
Cédula, Nombres, Apellidos y Correo electrónico son obligatorios y cada uno tiene un proceso de validación de acuerdo al tipo de dato.


Al momento de  registrar al empleado la API devuelve el nombre de usuario ,contraseña asignada, además de tener asignado al rol USER.

Con el usuario admin con rol ADMIN además se permite listar, editar y eliminar los empleados.

Tambien se permite filtrar por:

- Filtrar por estado de vacunación.
- Filtrar por tipo de vacuna.
- Filtrar por rango de fecha de vacunación (para las fechas el formato debe ser ingresado como YYYY-mm-dd).

Los usuarios con rol USER podran unicamente ver y actualizar su información. 
